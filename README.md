# CreditSystem
Проект специально для МТС Фентех Академия.
Структура проекта:
<br>
-src
<br>
 -main
 <br>
  -docker (для запуска docker образов)
  <br>
  -java
  <br>
   -component (для компонентов проекта)
   <br>
   -configuration
   <br>
   -constants
   <br>
   -controller
   <br>
   -dto
   <br>
   -entity
   <br>
    -kafka
    <br>
    -response
    <br>
    -tables
    <br>
   -exceptions
   <br>
    -handlers (handler для ошибок в представленных в папке)
    <br>
   -repository
   <br>
    -impl (реализации интерфейсов ниже)
    <br>
    -interfaces
    <br>
   -service
   <br>
    -impl(реализации интерфейсов ниже)
    <br>
    -interfaces
    <br>
     

Как запустить проект:
<br>
-Потребуется версия java не ниже 17.
<br>
-Зайдите в директорию проекта и запустите команду: mvnw package -DskipTests
<br>
-После скопируйте .jar файл из папки ...demo/target в ..demo/src/main/docker
<br>
-Находясь в этом пути пропишите docker-compose up.
<br>
-Приложение доступно будет по порту 8080.
